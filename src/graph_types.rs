use dnd5e::attack_sequence::*;
use dnd5e::dice::*;
use dnd5e::distribution::*;
use dnd5e::check_against_dc::CheckAgainstDC as ICheckAgainstDC;
use dnd5e::saving_throw::*;
use dnd5e::group_check::*;
use dnd5e::spell::*;
use dnd5e::typedefs::*;

use crate::graph_utils::*;
use crate::graph_parameters::*;
use crate::client_data_deserialization::*;

use wasm_bindgen::prelude::*;
use serde::Deserialize;

// A range of realistic values you might encounter in an actual game of D&D.
fn GetACs                         () -> Vec<AC                 > { ( 5..=30).map(AC                 ).collect() }
fn GetOpponentSavingThrowModifiers() -> Vec<SavingThrowModifier> { (-5..=20).map(SavingThrowModifier).collect() }
fn GetDCs                         () -> Vec<DC                 > { ( 5..=30).map(DC                 ).collect() }

pub mod PlotDie
{
    use super::*;

    #[wasm_bindgen]
    pub fn WASM_GetInputForChangeDetectorTests_DiceList() -> String
    {
r#"[
  { name: "6d2" , dice: "6d2"  },
  { name: "4d3" , dice: "4d3"  },
  { name: "3d4" , dice: "3d4"  },
  { name: "2d6" , dice: "2d6"  },
  { name: "1d12", dice: "1d12" },
]"#
        .to_string()
    }

    #[wasm_bindgen]
    pub fn WASM_ValidateInput_DiceList( input : &str ) -> Result<(), JsValue>
    {
        let _ = DeserializeClientDataVec_Die(input)?;
        Ok(())
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_Dice_ProbabilityEQValue;

    #[wasm_bindgen]
    impl WASM_Dice_ProbabilityEQValue
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList0()
        }

        pub fn GeneratePlot( input : &str ) -> Result<String, JsValue>
        {
            let someDice = DeserializeClientDataVec_Die(input)?;

            let plot : Plot2D<Value, Probability> = GeneratePlot2D_EntireDataSeries
            (
                "Probability Of Rolling Value On Die",
                "Value",
                "Probability of rolling X",
                0.0,
                1.0,
                &someDice,
                &|aDie|
                {
                    aDie.BuildProbabilityDistribution()
                        .iter()
                        .map(|(value, probability)| PlotPoint2D{ x : *value, y : *probability } )
                        .collect()
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_Dice_ProbabilityEQValue()
    {
        let expectedOutputJSON = r#"{"title":"Probability Of Rolling Value On Die","xAxisLabel":"Value","yAxisLabel":"Probability of rolling X","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"6d2","dataSeries":[{"x":6,"y":0.015625},{"x":7,"y":0.09375},{"x":8,"y":0.234375},{"x":9,"y":0.3125},{"x":10,"y":0.234375},{"x":11,"y":0.09375},{"x":12,"y":0.015625}]},{"label":"4d3","dataSeries":[{"x":4,"y":0.012345679012345678},{"x":5,"y":0.04938271604938271},{"x":6,"y":0.12345679012345678},{"x":7,"y":0.19753086419753085},{"x":8,"y":0.2345679012345679},{"x":9,"y":0.19753086419753085},{"x":10,"y":0.12345679012345678},{"x":11,"y":0.04938271604938271},{"x":12,"y":0.012345679012345678}]},{"label":"3d4","dataSeries":[{"x":3,"y":0.015625},{"x":4,"y":0.046875},{"x":5,"y":0.09375},{"x":6,"y":0.15625},{"x":7,"y":0.1875},{"x":8,"y":0.1875},{"x":9,"y":0.15625},{"x":10,"y":0.09375},{"x":11,"y":0.046875},{"x":12,"y":0.015625}]},{"label":"2d6","dataSeries":[{"x":2,"y":0.027777777777777776},{"x":3,"y":0.05555555555555555},{"x":4,"y":0.08333333333333333},{"x":5,"y":0.1111111111111111},{"x":6,"y":0.1388888888888889},{"x":7,"y":0.16666666666666666},{"x":8,"y":0.1388888888888889},{"x":9,"y":0.1111111111111111},{"x":10,"y":0.08333333333333333},{"x":11,"y":0.05555555555555555},{"x":12,"y":0.027777777777777776}]},{"label":"1d12","dataSeries":[{"x":1,"y":0.08333333333333333},{"x":2,"y":0.08333333333333333},{"x":3,"y":0.08333333333333333},{"x":4,"y":0.08333333333333333},{"x":5,"y":0.08333333333333333},{"x":6,"y":0.08333333333333333},{"x":7,"y":0.08333333333333333},{"x":8,"y":0.08333333333333333},{"x":9,"y":0.08333333333333333},{"x":10,"y":0.08333333333333333},{"x":11,"y":0.08333333333333333},{"x":12,"y":0.08333333333333333}]}]}"#;

        let outputJSON = WASM_Dice_ProbabilityEQValue::GeneratePlot( &WASM_GetInputForChangeDetectorTests_DiceList() ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_Dice_ProbabilityGEValue;

    #[wasm_bindgen]
    impl WASM_Dice_ProbabilityGEValue
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList0()
        }

        pub fn GeneratePlot( input : &str ) -> Result<String, JsValue>
        {
            let someDice = DeserializeClientDataVec_Die(input)?;

            let plot : Plot2D<Value, Probability> = GeneratePlot2D_EntireDataSeries
            (
                "Probability Of Rolling >= Value On Die",
                "Value",
                "Probability of rolling >= X",
                0.0,
                1.0,
                &someDice,
                &|aDie|
                {
                    aDie.BuildSurvivalProbabilityDistribution()
                        .iter()
                        .map(|(value, probability)| PlotPoint2D{ x : *value, y : *probability } )
                        .collect()
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_Dice_ProbabilityGEValue()
    {
        let expectedOutputJSON = r#"{"title":"Probability Of Rolling >= Value On Die","xAxisLabel":"Value","yAxisLabel":"Probability of rolling >= X","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"6d2","dataSeries":[{"x":6,"y":1.0},{"x":7,"y":0.984375},{"x":8,"y":0.890625},{"x":9,"y":0.65625},{"x":10,"y":0.34375},{"x":11,"y":0.109375},{"x":12,"y":0.015625}]},{"label":"4d3","dataSeries":[{"x":4,"y":1.0},{"x":5,"y":0.9876543209876543},{"x":6,"y":0.9382716049382716},{"x":7,"y":0.8148148148148148},{"x":8,"y":0.6172839506172839},{"x":9,"y":0.38271604938271603},{"x":10,"y":0.18518518518518517},{"x":11,"y":0.06172839506172839},{"x":12,"y":0.012345679012345678}]},{"label":"3d4","dataSeries":[{"x":3,"y":1.0},{"x":4,"y":0.984375},{"x":5,"y":0.9375},{"x":6,"y":0.84375},{"x":7,"y":0.6875},{"x":8,"y":0.5},{"x":9,"y":0.3125},{"x":10,"y":0.15625},{"x":11,"y":0.0625},{"x":12,"y":0.015625}]},{"label":"2d6","dataSeries":[{"x":2,"y":1.0},{"x":3,"y":0.9722222222222222},{"x":4,"y":0.9166666666666666},{"x":5,"y":0.8333333333333334},{"x":6,"y":0.7222222222222222},{"x":7,"y":0.5833333333333334},{"x":8,"y":0.4166666666666667},{"x":9,"y":0.2777777777777778},{"x":10,"y":0.16666666666666666},{"x":11,"y":0.08333333333333333},{"x":12,"y":0.027777777777777776}]},{"label":"1d12","dataSeries":[{"x":1,"y":1.0},{"x":2,"y":0.9166666666666666},{"x":3,"y":0.8333333333333334},{"x":4,"y":0.75},{"x":5,"y":0.6666666666666666},{"x":6,"y":0.5833333333333334},{"x":7,"y":0.5},{"x":8,"y":0.4166666666666667},{"x":9,"y":0.3333333333333333},{"x":10,"y":0.25},{"x":11,"y":0.16666666666666666},{"x":12,"y":0.08333333333333333}]}]}"#;

        let outputJSON = WASM_Dice_ProbabilityGEValue::GeneratePlot( &WASM_GetInputForChangeDetectorTests_DiceList() ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }
}

pub mod PlotDamageToCreature
{
    use super::*;

    #[derive(Deserialize)]
    pub enum DamageDealer
    {
        Attack(AttackSequence),
        Spell (Spell)
        // TODO: Add AttackSequence_SaveThenAC? Would need to rename "Attack" variant maybe?
    }

    fn CalculateDamageDistribution(aDamageDealer : &DamageDealer, anAC : AC, aSavingThrow : &SavingThrow) -> Die
    {
        match aDamageDealer
        {
            DamageDealer::Attack(attackSequence) => attackSequence.CalculateDamageDistribution(anAC),
            DamageDealer::Spell (spell)          => spell         .CalculateDamageDistribution(aSavingThrow)
        }
    }

    fn FilterNonSpells( someDamageDealers : Vec<(String, DamageDealer)> ) -> Vec<(String, Spell)>
    {
        let IsSpell = |x : (String, DamageDealer)|
        {
            match x.1
            {
                DamageDealer::Attack(_)     => None,
                DamageDealer::Spell (spell) => Some( (x.0, spell) )
            }
        };

        // Remove all the non Spell objects from a list of DamageDealers.
        someDamageDealers.into_iter().filter_map(IsSpell).collect()
    }

    fn FilterNonAttacks( someDamageDealers : Vec<(String, DamageDealer)> ) -> Vec<(String, AttackSequence)>
    {
        let IsAttackSequence = |x : (String, DamageDealer)|
        {
            match x.1
            {
                DamageDealer::Attack(attackSequence) => Some( (x.0, attackSequence) ),
                DamageDealer::Spell (_)              => None
            }
        };

        // Remove all the non Attack objects from a list of DamageDealers.
        someDamageDealers.into_iter().filter_map(IsAttackSequence).collect()
    }

    #[wasm_bindgen]
    pub fn WASM_GetInputForChangeDetectorTests_DamageDealerList() -> String
    {
r#"[
  { name: "Eldritch Blast"      , Attack: {attacks: [{ attackMod: "3 + 3", damageMod: 3, damageDice: "1d10", rollStyle: "Normal", numAttacks: 2 }],                             } },
  { name: "Fire Bolt (2d10)"    , Attack: {attacks: [{ attackMod: "3 + 3", damageMod: 0, damageDice: "2d10", rollStyle: "Normal", numAttacks: 1 }],                             } },
  { name: "Sneak Attack"        , Attack: {attacks: [{ attackMod: "3 + 3", damageMod: 0, damageDice: "1d8" , rollStyle: "Normal", numAttacks: 1 }], oncePerTurnDamageDice: "3d6"} },
  { name: "Toll the Dead (2d12)", Spell : {spellType: "Cantrip", damage: "2d12"   , dc: 14} },
  { name: "Magic Missile"       , Spell : {spellType: "AutoHit", damage: "3d4 + 3", dc: 14} },
]"#
        .to_string()
    }

    #[wasm_bindgen]
    pub fn WASM_ValidateInput_DamageDealerList( input : &str ) -> Result<(), JsValue>
    {
        let _ = DeserializeClientDataVec::<DamageDealer>(input)?;
        Ok(())
    }

    #[wasm_bindgen]
    pub fn WASM_ValidateInput_HasSpell( input : &str ) -> Result<bool, JsValue>
    {
        let someDamageDealers = DeserializeClientDataVec::<DamageDealer>(input)?;

        let someSpells = FilterNonSpells( someDamageDealers );

        // Was there at least one Spell in the input list?
        Ok( !someSpells.is_empty() )
    }

    #[wasm_bindgen]
    pub fn WASM_ValidateInput_HasAttack( input : &str ) -> Result<bool, JsValue>
    {
        let someDamageDealers = DeserializeClientDataVec::<DamageDealer>(input)?;

        let someAttackSequences = FilterNonAttacks( someDamageDealers );

        // Was there at least one attack in the input list?
        Ok( !someAttackSequences.is_empty() )
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_Creature_TurnsToKill;

    #[wasm_bindgen]
    impl WASM_Creature_TurnsToKill
    {
        pub fn GetPlotParameters() -> JsValue
        {
            // TODO: Support a list of hardcoded monsters?
            //       Would be tough because setting one param would mean changing the value of the others.
            //       Could make a separate chart?
            PlotParameterList4( PlotParameter_Health             ("Creature Health"               )
                              , PlotParameter_AC                 ("Creature AC"                   )
                              , PlotParameter_SavingThrowModifier("Creature Saving Throw Modifier")
                              , PlotParameter_RollStyle          ("Creature Saving Throw Type"    ) )
        }

        pub fn GeneratePlot( input : &str, aHealthThreshold : Value, aRawAC : Value, aRawOpponentSavingThrowModifier : Value, anOpponentSavingThrowRollStyleStr : &str  ) -> Result<String, JsValue>
        {
            let someDamageDealers = DeserializeClientDataVec::<DamageDealer>(input)?;

            let anAC = AC(aRawAC);

            let aNumberOfTurns = std::num::NonZeroU8::new(8).unwrap();

            let anOpponentSavingThrowModifier = SavingThrowModifier(aRawOpponentSavingThrowModifier);

            let anOpponentSavingThrowRollStyle : RollStyle = DeserializeJSON::<RollStyle>(anOpponentSavingThrowRollStyleStr)?;

            let opponentSavingThrow = SavingThrow::FromMod(anOpponentSavingThrowRollStyle, anOpponentSavingThrowModifier);

            let plotTitleSavingThrowRollStylePortion = match anOpponentSavingThrowRollStyle
            {
                // "Normal" is the expected default state so don't include it in
                // the title since it would just add noise and hurt readability.
                RollStyle::Normal               => "",
                RollStyle::Advantage            => " (Advantage)",
                RollStyle::SuperAdvantage       => " (SuperAdvantage)",
                RollStyle::Disadvantage         => " (Disadvantage)",
                RollStyle::Normal_HalflingLucky => " (Halfling Lucky)",
            };

            let plot : Plot2D<u8, Probability> = GeneratePlot2D_EntireDataSeries
            (
                &format!("Number Of Rounds To Kill A {} HP Creature with {} AC and a {:+} Save{}", aHealthThreshold, anAC, anOpponentSavingThrowModifier, plotTitleSavingThrowRollStylePortion ),
                "# of rounds",
                "Probability of killing in <= X rounds",
                0.0,
                1.0,
                &someDamageDealers,
                &|aDamageDealer|
                {
                    GenerateDataSeriesFromSequentialTrials
                    (
                        &CalculateDamageDistribution(aDamageDealer, anAC, &opponentSavingThrow),
                        _1d0(),
                        aNumberOfTurns,
                        &|accumulatedDistribution : &Die, _currentTrialNumber : u8| { accumulatedDistribution.ChanceOfGE_Value(aHealthThreshold) }
                    )
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_Creature_TurnsToKill()
    {
        let expectedOutputJSON = r##"{"title":"Number Of Rounds To Kill A 30 HP Creature with 10 AC and a +0 Save","xAxisLabel":"# of rounds","yAxisLabel":"Probability of killing in <= X rounds","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"Eldritch Blast","dataSeries":[{"x":1,"y":0.00772125},{"x":2,"y":0.530224644690625},{"x":3,"y":0.9251459124345093},{"x":4,"y":0.9930409698512536},{"x":5,"y":0.9995220312776376},{"x":6,"y":0.9999729453005045},{"x":7,"y":0.9999986613433591},{"x":8,"y":0.9999999400158427}]},{"label":"Fire Bolt (2d10)","dataSeries":[{"x":1,"y":0.004985},{"x":2,"y":0.122632266475},{"x":3,"y":0.5049154749043401},{"x":4,"y":0.7931663488313744},{"x":5,"y":0.9284689778986132},{"x":6,"y":0.9782172581876525},{"x":7,"y":0.9939449209383039},{"x":8,"y":0.9984284698689811}]},{"label":"Sneak Attack","dataSeries":[{"x":1,"y":0.026844015239197532},{"x":2,"y":0.43355065589225833},{"x":3,"y":0.8035322023803487},{"x":4,"y":0.9459556482329038},{"x":5,"y":0.9869416886494543},{"x":6,"y":0.9970953833028378},{"x":7,"y":0.999389955019738},{"x":8,"y":0.9998771675048957}]},{"label":"Toll the Dead (2d12)","dataSeries":[{"x":1,"y":0.0},{"x":2,"y":0.13192937403549382},{"x":3,"y":0.3762920338822981},{"x":4,"y":0.6066458680992173},{"x":5,"y":0.7732291388087381},{"x":6,"y":0.8774618026626914},{"x":7,"y":0.9369353310222004},{"x":8,"y":0.9687543774202689}]},{"label":"Magic Missile","dataSeries":[{"x":1,"y":0.0},{"x":2,"y":0.000244140625},{"x":3,"y":0.721832275390625},{"x":4,"y":0.9996404647827148},{"x":5,"y":1.0},{"x":6,"y":1.0},{"x":7,"y":1.0},{"x":8,"y":1.0}]}]}"##;

        let outputJSON = WASM_Creature_TurnsToKill::GeneratePlot( &WASM_GetInputForChangeDetectorTests_DamageDealerList(), 30, 10, 0, "\"Normal\"" ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_Creature_DamageProbability;

    #[wasm_bindgen]
    impl WASM_Creature_DamageProbability
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList3( PlotParameter_AC                 ("Creature AC"                   )
                              , PlotParameter_SavingThrowModifier("Creature Saving Throw Modifier")
                              , PlotParameter_RollStyle          ("Creature Saving Throw Type"    ) )
        }

        pub fn GeneratePlot( input : &str, aRawAC : Value, aRawOpponentSavingThrowModifier : Value, anOpponentSavingThrowRollStyleStr : &str ) -> Result<String, JsValue>
        {
            let someDamageDealers = DeserializeClientDataVec::<DamageDealer>(input)?;

            let anAC = AC(aRawAC);

            let anOpponentSavingThrowModifier = SavingThrowModifier(aRawOpponentSavingThrowModifier);

            let anOpponentSavingThrowRollStyle : RollStyle = DeserializeJSON::<RollStyle>(anOpponentSavingThrowRollStyleStr)?;

            let opponentSavingThrow = SavingThrow::FromMod(anOpponentSavingThrowRollStyle, anOpponentSavingThrowModifier);

            let plotTitleSavingThrowRollStylePortion = match anOpponentSavingThrowRollStyle
            {
                // "Normal" is the expected default state so don't include it in
                // the title since it would just add noise and hurt readability.
                RollStyle::Normal               => "",
                RollStyle::Advantage            => " (Advantage)",
                RollStyle::SuperAdvantage       => " (SuperAdvantage)",
                RollStyle::Disadvantage         => " (Disadvantage)",
                RollStyle::Normal_HalflingLucky => " (Halfling Lucky)",
            };

            let plot : Plot2D<Value, Probability> = GeneratePlot2D_EntireDataSeries
            (
                &format!("Damage Probability Against A Creature With {} AC and a {:+} Save{}", anAC, anOpponentSavingThrowModifier, plotTitleSavingThrowRollStylePortion ),
                "Damage",
                "Probability of doing >= X damage",
                0.0,
                1.0,
                &someDamageDealers,
                &|aDamageDealer|
                {
                    let survivalProbDist = CalculateDamageDistribution(aDamageDealer, anAC, &opponentSavingThrow).BuildSurvivalProbabilityDistribution();

                    survivalProbDist
                        .iter()
                        .map(|(damageValue, probability)| PlotPoint2D{ x : *damageValue, y : *probability } )
                        .collect()
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_Creature_DamageProbability()
    {
        let expectedOutputJSON = r#"{"title":"Damage Probability Against A Creature With 10 AC and a +0 Save","xAxisLabel":"Damage","yAxisLabel":"Probability of doing >= X damage","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"Eldritch Blast","dataSeries":[{"x":0,"y":1.0},{"x":4,"y":0.9775},{"x":5,"y":0.9535},{"x":6,"y":0.92935},{"x":7,"y":0.90505},{"x":8,"y":0.8806},{"x":9,"y":0.8496},{"x":10,"y":0.81197},{"x":11,"y":0.76762975},{"x":12,"y":0.71649875},{"x":13,"y":0.65849625},{"x":14,"y":0.59354125},{"x":15,"y":0.5455525},{"x":16,"y":0.4907485},{"x":17,"y":0.4290475},{"x":18,"y":0.3603675},{"x":19,"y":0.29742625},{"x":20,"y":0.24038125},{"x":21,"y":0.18939075},{"x":22,"y":0.14461375},{"x":23,"y":0.10621},{"x":24,"y":0.07434},{"x":25,"y":0.049165},{"x":26,"y":0.030697},{"x":27,"y":0.01909875},{"x":28,"y":0.01453375},{"x":29,"y":0.01076625},{"x":30,"y":0.00772125},{"x":31,"y":0.005323},{"x":32,"y":0.003495},{"x":33,"y":0.00216},{"x":34,"y":0.00124},{"x":35,"y":0.00065625},{"x":36,"y":0.00032925},{"x":37,"y":0.00017875},{"x":38,"y":0.00012375},{"x":39,"y":0.0000825},{"x":40,"y":0.0000525},{"x":41,"y":0.0000315},{"x":42,"y":0.0000175},{"x":43,"y":8.75e-6},{"x":44,"y":3.75e-6},{"x":45,"y":1.25e-6},{"x":46,"y":2.5e-7}]},{"label":"Fire Bolt (2d10)","dataSeries":[{"x":0,"y":1.0},{"x":2,"y":0.85},{"x":3,"y":0.842},{"x":4,"y":0.826},{"x":5,"y":0.801995},{"x":6,"y":0.769975},{"x":7,"y":0.729925},{"x":8,"y":0.681825},{"x":9,"y":0.62565},{"x":10,"y":0.56137},{"x":11,"y":0.48895},{"x":12,"y":0.40835},{"x":13,"y":0.335525},{"x":14,"y":0.270425},{"x":15,"y":0.213015},{"x":16,"y":0.163275},{"x":17,"y":0.1212},{"x":18,"y":0.0868},{"x":19,"y":0.0601},{"x":20,"y":0.04114},{"x":21,"y":0.029975},{"x":22,"y":0.026675},{"x":23,"y":0.023325},{"x":24,"y":0.020025},{"x":25,"y":0.01686},{"x":26,"y":0.0139},{"x":27,"y":0.0112},{"x":28,"y":0.0088},{"x":29,"y":0.006725},{"x":30,"y":0.004985},{"x":31,"y":0.003575},{"x":32,"y":0.002475},{"x":33,"y":0.00165},{"x":34,"y":0.00105},{"x":35,"y":0.00063},{"x":36,"y":0.00035},{"x":37,"y":0.000175},{"x":38,"y":0.000075},{"x":39,"y":0.000025},{"x":40,"y":5e-6}]},{"label":"Sneak Attack","dataSeries":[{"x":0,"y":1.0},{"x":4,"y":0.85},{"x":5,"y":0.8495370370370371},{"x":6,"y":0.8476851851851852},{"x":7,"y":0.8430555555555556},{"x":8,"y":0.8337962962962963},{"x":9,"y":0.8175925758476937},{"x":10,"y":0.7916665159625772},{"x":11,"y":0.7541659131462192},{"x":12,"y":0.704163903758359},{"x":13,"y":0.6421213409047067},{"x":14,"y":0.569885856722608},{"x":15,"y":0.4906905562789352},{"x":16,"y":0.4091524100597994},{"x":17,"y":0.3298816403570816},{"x":18,"y":0.25748018073774004},{"x":19,"y":0.19515184944058642},{"x":20,"y":0.144702550315072},{"x":21,"y":0.10654201429076646},{"x":22,"y":0.0796873492959105},{"x":23,"y":0.062231294608410495},{"x":24,"y":0.05134885183577675},{"x":25,"y":0.044693390855409805},{"x":26,"y":0.04040334777413409},{"x":27,"y":0.03710728188764575},{"x":28,"y":0.03392615298675412},{"x":29,"y":0.03047216595936214},{"x":30,"y":0.026844015239197532},{"x":31,"y":0.02315598476080247},{"x":32,"y":0.01952783404063786},{"x":33,"y":0.016073847013245886},{"x":34,"y":0.012892718112354252},{"x":35,"y":0.010059615188828875},{"x":36,"y":0.007621423959405007},{"x":37,"y":0.005595592608667695},{"x":38,"y":0.00397240909529321},{"x":39,"y":0.0027200581114969137},{"x":40,"y":0.0017913190425668723},{"x":41,"y":0.001130783018261317},{"x":42,"y":0.0006814838927469136},{"x":43,"y":0.0003901896326303155},{"x":44,"y":0.00021095223551097393},{"x":45,"y":0.00010684919945987655},{"x":46,"y":0.000050184461805555553},{"x":47,"y":0.000021550684799382716},{"x":48,"y":8.288724922839506e-6},{"x":49,"y":2.762908307613169e-6},{"x":50,"y":7.535204475308642e-7},{"x":51,"y":1.5070408950617284e-7},{"x":52,"y":1.6744898834019205e-8}]},{"label":"Toll the Dead (2d12)","dataSeries":[{"x":0,"y":1.0},{"x":2,"y":0.65},{"x":3,"y":0.6454861111111111},{"x":4,"y":0.6364583333333333},{"x":5,"y":0.6229166666666667},{"x":6,"y":0.6048611111111111},{"x":7,"y":0.5822916666666667},{"x":8,"y":0.5552083333333333},{"x":9,"y":0.5236111111111111},{"x":10,"y":0.4875},{"x":11,"y":0.446875},{"x":12,"y":0.4017361111111111},{"x":13,"y":0.35208333333333336},{"x":14,"y":0.29791666666666666},{"x":15,"y":0.2482638888888889},{"x":16,"y":0.203125},{"x":17,"y":0.1625},{"x":18,"y":0.12638888888888888},{"x":19,"y":0.09479166666666666},{"x":20,"y":0.06770833333333333},{"x":21,"y":0.04513888888888889},{"x":22,"y":0.027083333333333334},{"x":23,"y":0.013541666666666667},{"x":24,"y":0.0045138888888888885}]},{"label":"Magic Missile","dataSeries":[{"x":6,"y":1.0},{"x":7,"y":0.984375},{"x":8,"y":0.9375},{"x":9,"y":0.84375},{"x":10,"y":0.6875},{"x":11,"y":0.5},{"x":12,"y":0.3125},{"x":13,"y":0.15625},{"x":14,"y":0.0625},{"x":15,"y":0.015625}]}]}"#;

        let outputJSON = WASM_Creature_DamageProbability::GeneratePlot( &WASM_GetInputForChangeDetectorTests_DamageDealerList(), 10, 0, "\"Normal\"" ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_Spell_MeanDamage;

    #[wasm_bindgen]
    impl WASM_Spell_MeanDamage
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList1( PlotParameter_RollStyle("Opponent Saving Throw Type") )
        }

        pub fn GeneratePlot( input : &str, anOpponentSavingThrowRollStyleStr : &str ) -> Result<String, JsValue>
        {
            let someDamageDealers = DeserializeClientDataVec::<DamageDealer>(input)?;

            // This plot only makes sense for spells. However, for convenience we want to
            // allow clients provide the same input data that they can for any of the other
            // plots in this module, to avoid requiring a special case for a single plot.
            // So we take their input and just remove anything that isn't a spell. If this
            // leaves us an empty list then that's ok, the graph will just be empty. It's
            // up to the client to prevent that if they want. Otherwise we have to support
            // optional plots which is more work than we want to deal with right now.
            let someSpells = FilterNonSpells( someDamageDealers );

            let someOpponentSavingThrowModifiers = GetOpponentSavingThrowModifiers();

            let anOpponentSavingThrowRollStyle : RollStyle = DeserializeJSON::<RollStyle>(anOpponentSavingThrowRollStyleStr)?;

            let plot : Plot2D<SavingThrowModifier, MeanValue> = GeneratePlot2D_PointByPoint
            (
                "Mean Spell Damage Against Opponent Saving Throw Modifier",
                "Opponent saving throw modifier",
                "Mean damage",
                MeanValue(std::f64::MAX), // Intentionally reversed so that ChartJS ignores the suggestion.
                MeanValue(std::f64::MIN), // Intentionally reversed so that ChartJS ignores the suggestion.
                &someSpells,
                &someOpponentSavingThrowModifiers,
                &|aSpell, &anOpponentSavingThrowModifier|
                {
                    let opponentSavingThrow = SavingThrow::FromMod(anOpponentSavingThrowRollStyle, anOpponentSavingThrowModifier);

                    aSpell.ExpectedDamage(&opponentSavingThrow)
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_Spell_MeanDamage()
    {
        let expectedOutputJSON = r#"{"title":"Mean Spell Damage Against Opponent Saving Throw Modifier","xAxisLabel":"Opponent saving throw modifier","yAxisLabel":"Mean damage","yAxisSuggestedMin":1.7976931348623157e308,"yAxisSuggestedMax":-1.7976931348623157e308,"data2D":[{"label":"Toll the Dead (2d12)","dataSeries":[{"x":-5,"y":11.7},{"x":-4,"y":11.05},{"x":-3,"y":10.4},{"x":-2,"y":9.75},{"x":-1,"y":9.1},{"x":0,"y":8.45},{"x":1,"y":7.8},{"x":2,"y":7.15},{"x":3,"y":6.5},{"x":4,"y":5.85},{"x":5,"y":5.2},{"x":6,"y":4.55},{"x":7,"y":3.9},{"x":8,"y":3.25},{"x":9,"y":2.6},{"x":10,"y":1.95},{"x":11,"y":1.3},{"x":12,"y":0.65},{"x":13,"y":0.0},{"x":14,"y":0.0},{"x":15,"y":0.0},{"x":16,"y":0.0},{"x":17,"y":0.0},{"x":18,"y":0.0},{"x":19,"y":0.0},{"x":20,"y":0.0}]},{"label":"Magic Missile","dataSeries":[{"x":-5,"y":10.5},{"x":-4,"y":10.5},{"x":-3,"y":10.5},{"x":-2,"y":10.5},{"x":-1,"y":10.5},{"x":0,"y":10.5},{"x":1,"y":10.5},{"x":2,"y":10.5},{"x":3,"y":10.5},{"x":4,"y":10.5},{"x":5,"y":10.5},{"x":6,"y":10.5},{"x":7,"y":10.5},{"x":8,"y":10.5},{"x":9,"y":10.5},{"x":10,"y":10.5},{"x":11,"y":10.5},{"x":12,"y":10.5},{"x":13,"y":10.5},{"x":14,"y":10.5},{"x":15,"y":10.5},{"x":16,"y":10.5},{"x":17,"y":10.5},{"x":18,"y":10.5},{"x":19,"y":10.5},{"x":20,"y":10.5}]}]}"#;

        let outputJSON = WASM_Spell_MeanDamage::GeneratePlot( &WASM_GetInputForChangeDetectorTests_DamageDealerList(), "\"Normal\"" ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_AttackSequence_MeanDamage;

    #[wasm_bindgen]
    impl WASM_AttackSequence_MeanDamage
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList0()
        }

        pub fn GeneratePlot( input : &str ) -> Result<String, JsValue>
        {
            let someDamageDealers = DeserializeClientDataVec::<DamageDealer>(input)?;

            // This plot only makes sense for attacks. However, for convenience we want to
            // allow clients provide the same input data that they can for any of the other
            // plots in this module, to avoid requiring a special case for a single plot.
            // So we take their input and just remove anything that isn't a spell. If this
            // leaves us an empty list then that's ok, the graph will just be empty. It's
            // up to the client to prevent that if they want. Otherwise we have to support
            // optional plots which is more work than we want to deal with right now.
            let someAttackSequences = FilterNonAttacks( someDamageDealers );

            let someACs = GetACs();

            let plot : Plot2D<AC, MeanValue> = GeneratePlot2D_PointByPoint
            (
                "Mean Damage Against AC",
                "AC",
                "Mean damage",
                MeanValue(std::f64::MAX), // Intentionally reversed so that ChartJS ignores the suggestion.
                MeanValue(std::f64::MIN), // Intentionally reversed so that ChartJS ignores the suggestion.
                &someAttackSequences,
                &someACs,
                &|anAttackSequence, anAC|
                {
                    anAttackSequence.ExpectedDamage(*anAC)
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_AttackSequence_MeanDamage()
    {
        let expectedOutputJSON = r#"{"title":"Mean Damage Against AC","xAxisLabel":"AC","yAxisLabel":"Mean damage","yAxisSuggestedMin":1.7976931348623157e308,"yAxisSuggestedMax":-1.7976931348623157e308,"data2D":[{"label":"Eldritch Blast","dataSeries":[{"x":5,"y":16.7},{"x":6,"y":16.7},{"x":7,"y":16.7},{"x":8,"y":16.7},{"x":9,"y":15.85},{"x":10,"y":15.0},{"x":11,"y":14.15},{"x":12,"y":13.3},{"x":13,"y":12.45},{"x":14,"y":11.6},{"x":15,"y":10.75},{"x":16,"y":9.9},{"x":17,"y":9.05},{"x":18,"y":8.2},{"x":19,"y":7.35},{"x":20,"y":6.5},{"x":21,"y":5.65},{"x":22,"y":4.8},{"x":23,"y":3.95},{"x":24,"y":3.1},{"x":25,"y":2.25},{"x":26,"y":1.4},{"x":27,"y":1.4},{"x":28,"y":1.4},{"x":29,"y":1.4},{"x":30,"y":1.4}]},{"label":"Fire Bolt (2d10)","dataSeries":[{"x":5,"y":11.0},{"x":6,"y":11.0},{"x":7,"y":11.0},{"x":8,"y":11.0},{"x":9,"y":10.45},{"x":10,"y":9.9},{"x":11,"y":9.35},{"x":12,"y":8.8},{"x":13,"y":8.25},{"x":14,"y":7.7},{"x":15,"y":7.15},{"x":16,"y":6.6},{"x":17,"y":6.05},{"x":18,"y":5.5},{"x":19,"y":4.95},{"x":20,"y":4.4},{"x":21,"y":3.85},{"x":22,"y":3.3},{"x":23,"y":2.75},{"x":24,"y":2.2},{"x":25,"y":1.65},{"x":26,"y":1.1},{"x":27,"y":1.1},{"x":28,"y":1.1},{"x":29,"y":1.1},{"x":30,"y":1.1}]},{"label":"Sneak Attack","dataSeries":[{"x":5,"y":15.0},{"x":6,"y":15.0},{"x":7,"y":15.0},{"x":8,"y":15.0},{"x":9,"y":14.25},{"x":10,"y":13.5},{"x":11,"y":12.75},{"x":12,"y":12.0},{"x":13,"y":11.25},{"x":14,"y":10.5},{"x":15,"y":9.75},{"x":16,"y":9.0},{"x":17,"y":8.25},{"x":18,"y":7.5},{"x":19,"y":6.75},{"x":20,"y":6.0},{"x":21,"y":5.25},{"x":22,"y":4.5},{"x":23,"y":3.75},{"x":24,"y":3.0},{"x":25,"y":2.25},{"x":26,"y":1.5},{"x":27,"y":1.5},{"x":28,"y":1.5},{"x":29,"y":1.5},{"x":30,"y":1.5}]}]}"#;

        let outputJSON = WASM_AttackSequence_MeanDamage::GeneratePlot( &WASM_GetInputForChangeDetectorTests_DamageDealerList() ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }
}

pub mod PlotCheckAgainstDC
{
    use super::*;

    #[derive(Deserialize)]
    pub enum CheckAgainstDC
    {
        SavingThrow(SavingThrow),
        GroupCheck (GroupCheck)
    }

    fn ChanceToPass(aCheckAgainstDC : &CheckAgainstDC, aDC : DC) -> Probability
    {
        match aCheckAgainstDC
        {
            CheckAgainstDC::SavingThrow(savingThrow) => savingThrow.ChanceToPass(aDC),
            CheckAgainstDC::GroupCheck (groupCheck)  => groupCheck .ChanceToPass(aDC)
        }
    }

    fn CalculateSuccessDistribution(aCheckAgainstDC : &CheckAgainstDC, aDC : DC) -> Die
    {
        match aCheckAgainstDC
        {
            CheckAgainstDC::SavingThrow(savingThrow) => savingThrow.CalculateSuccessDistribution(aDC),
            CheckAgainstDC::GroupCheck (groupCheck)  => groupCheck .CalculateSuccessDistribution(aDC)
        }
    }

    fn FilterNonSavingThrows( someChecksAgainstDC : Vec<(String, CheckAgainstDC)> ) -> Vec<(String, SavingThrow)>
    {
        let IsSavingThrow = |x : (String, CheckAgainstDC)|
        {
            match x.1
            {
                CheckAgainstDC::SavingThrow(savingThrow) => Some( (x.0, savingThrow) ),
                CheckAgainstDC::GroupCheck (_)           => None
            }
        };

        // Remove all the non SavingThrow objects from a list of CheckAgainstDCs.
        someChecksAgainstDC.into_iter().filter_map(IsSavingThrow).collect()
    }

    #[wasm_bindgen]
    pub fn WASM_GetInputForChangeDetectorTests_CheckAgainstDCList() -> String
    {
r#"[
  { name: "1 Rogue"            , SavingThrow: {modifier: "6"} },

  { name: "4 Rogues (Majority)", GroupCheck : { group: [{modifier: "6"}, {modifier: "6"}, {modifier: "6"}, {modifier: "6"}], passCriteria: "MajorityPass" } },

  { name: "4 Rogues (Average)" , GroupCheck : { group: [{modifier: "6"}, {modifier: "6"}, {modifier: "6"}, {modifier: "6"}], passCriteria: "AverageRoll"  } },

  { name: "One of Each"        , GroupCheck : { group: [ {modifier: "0", rollStyle: "Normal"      }
                                                       , {modifier: "0", rollStyle: "Advantage"   }
                                                       , {modifier: "0", rollStyle: "Disadvantage"} ] } },
]"#
        .to_string()
    }

    #[wasm_bindgen]
    pub fn WASM_ValidateInput_CheckAgainstDCList( input : &str ) -> Result<(), JsValue>
    {
        let _ = DeserializeClientDataVec::<CheckAgainstDC>(input)?;
        Ok(())
    }

    #[wasm_bindgen]
    pub fn WASM_ValidateInput_HasSavingThrow( input : &str ) -> Result<bool, JsValue>
    {
        let someChecksAgainstDC = DeserializeClientDataVec::<CheckAgainstDC>(input)?;

        let someSavingThrows = FilterNonSavingThrows(someChecksAgainstDC);

        // Was there at least one saving throw in the input list?
        Ok( !someSavingThrows.is_empty() )
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_CheckAgainstDC_SuccessProbabilityDC;

    #[wasm_bindgen]
    impl WASM_CheckAgainstDC_SuccessProbabilityDC
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList0()
        }

        pub fn GeneratePlot( input : &str ) -> Result<String, JsValue>
        {
            let someChecksAgainstDC = DeserializeClientDataVec::<CheckAgainstDC>(input)?;

            let someDCs = GetDCs();

            let plot : Plot2D<DC, Probability> = GeneratePlot2D_PointByPoint
            (
                "Probability Of Passing Check Against DCs",
                "DC",
                "Probability of passing",
                0.0,
                1.0,
                &someChecksAgainstDC,
                &someDCs,
                &|aCheckAgainstDC, aDC|
                {
                    ChanceToPass(aCheckAgainstDC, *aDC)
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_CheckAgainstDC_SuccessProbabilityDC()
    {
        let expectedOutputJSON = r#"{"title":"Probability Of Passing Check Against DCs","xAxisLabel":"DC","yAxisLabel":"Probability of passing","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"1 Rogue","dataSeries":[{"x":5,"y":1.0},{"x":6,"y":1.0},{"x":7,"y":1.0},{"x":8,"y":0.95},{"x":9,"y":0.9},{"x":10,"y":0.85},{"x":11,"y":0.8},{"x":12,"y":0.75},{"x":13,"y":0.7},{"x":14,"y":0.65},{"x":15,"y":0.6},{"x":16,"y":0.55},{"x":17,"y":0.5},{"x":18,"y":0.45},{"x":19,"y":0.4},{"x":20,"y":0.35},{"x":21,"y":0.3},{"x":22,"y":0.25},{"x":23,"y":0.2},{"x":24,"y":0.15},{"x":25,"y":0.1},{"x":26,"y":0.05},{"x":27,"y":0.0},{"x":28,"y":0.0},{"x":29,"y":0.0},{"x":30,"y":0.0}]},{"label":"4 Rogues (Majority)","dataSeries":[{"x":5,"y":1.0},{"x":6,"y":1.0},{"x":7,"y":1.0},{"x":8,"y":0.99951875},{"x":9,"y":0.9963},{"x":10,"y":0.98801875},{"x":11,"y":0.9728},{"x":12,"y":0.94921875},{"x":13,"y":0.9163},{"x":14,"y":0.87351875},{"x":15,"y":0.8208},{"x":16,"y":0.75851875},{"x":17,"y":0.6875},{"x":18,"y":0.60901875},{"x":19,"y":0.5248},{"x":20,"y":0.43701875},{"x":21,"y":0.3483},{"x":22,"y":0.26171875},{"x":23,"y":0.1808},{"x":24,"y":0.10951875},{"x":25,"y":0.0523},{"x":26,"y":0.01401875},{"x":27,"y":0.0},{"x":28,"y":0.0},{"x":29,"y":0.0},{"x":30,"y":0.0}]},{"label":"4 Rogues (Average)","dataSeries":[{"x":5,"y":1.0},{"x":6,"y":1.0},{"x":7,"y":1.0},{"x":8,"y":0.99978125},{"x":9,"y":0.9979375},{"x":10,"y":0.99146875},{"x":11,"y":0.975775},{"x":12,"y":0.94465625},{"x":13,"y":0.8911875},{"x":14,"y":0.81159375},{"x":15,"y":0.706875},{"x":16,"y":0.58283125},{"x":17,"y":0.4500625},{"x":18,"y":0.32265625},{"x":19,"y":0.212375},{"x":20,"y":0.12621875},{"x":21,"y":0.0663875},{"x":22,"y":0.03028125},{"x":23,"y":0.011375},{"x":24,"y":0.00309375},{"x":25,"y":0.0004375},{"x":26,"y":6.25e-6},{"x":27,"y":0.0},{"x":28,"y":0.0},{"x":29,"y":0.0},{"x":30,"y":0.0}]},{"label":"One of Each","dataSeries":[{"x":5,"y":0.91136},{"x":6,"y":0.861328125},{"x":7,"y":0.80164},{"x":8,"y":0.733776875},{"x":9,"y":0.65952},{"x":10,"y":0.580875625},{"x":11,"y":0.5},{"x":12,"y":0.419124375},{"x":13,"y":0.34048},{"x":14,"y":0.266223125},{"x":15,"y":0.19836},{"x":16,"y":0.138671875},{"x":17,"y":0.08864},{"x":18,"y":0.049370625},{"x":19,"y":0.02152},{"x":20,"y":0.005219375},{"x":21,"y":0.0},{"x":22,"y":0.0},{"x":23,"y":0.0},{"x":24,"y":0.0},{"x":25,"y":0.0},{"x":26,"y":0.0},{"x":27,"y":0.0},{"x":28,"y":0.0},{"x":29,"y":0.0},{"x":30,"y":0.0}]}]}"#;

        let outputJSON = WASM_CheckAgainstDC_SuccessProbabilityDC::GeneratePlot( &WASM_GetInputForChangeDetectorTests_CheckAgainstDCList() ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_CheckAgainstDC_PassAllProbability;

    #[wasm_bindgen]
    impl WASM_CheckAgainstDC_PassAllProbability
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList1( PlotParameter_DC("DC") )
        }

        pub fn GeneratePlot( input : &str, aRawDC : Value ) -> Result<String, JsValue>
        {
            let someChecksAgainstDC = DeserializeClientDataVec::<CheckAgainstDC>(input)?;

            let aNumberOfSavesToMake = std::num::NonZeroU8::new(8).unwrap();

            let aDC = DC(aRawDC);

            let plot : Plot2D<u8, Probability> = GeneratePlot2D_EntireDataSeries
            (
                &format!("Probability Of Passing Checks Against DC {}", aDC),
                "# of checks to pass in a row",
                "Probability of passing every check",
                0.0,
                1.0,
                &someChecksAgainstDC,
                &|aCheckAgainstDC|
                {
                    GenerateDataSeriesFromSequentialTrials
                    (
                        &CalculateSuccessDistribution(aCheckAgainstDC, aDC),
                        _1d0(),
                        aNumberOfSavesToMake,
                        &|accumulatedDistribution : &Die, currentTrialNumber : u8| { accumulatedDistribution.ChanceOfEQ_Value( Value::from(currentTrialNumber) ) }
                    )
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_CheckAgainstDC_PassAllProbability()
    {
        let expectedOutputJSON = r##"{"title":"Probability Of Passing Checks Against DC 10","xAxisLabel":"# of checks to pass in a row","yAxisLabel":"Probability of passing every check","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"1 Rogue","dataSeries":[{"x":1,"y":0.85},{"x":2,"y":0.7225},{"x":3,"y":0.614125},{"x":4,"y":0.52200625},{"x":5,"y":0.4437053125},{"x":6,"y":0.377149515625},{"x":7,"y":0.32057708828125},{"x":8,"y":0.2724905250390625}]},{"label":"4 Rogues (Majority)","dataSeries":[{"x":1,"y":0.98801875},{"x":2,"y":0.9761810503515626},{"x":3,"y":0.9644851811420379},{"x":4,"y":0.9529294430654798},{"x":5,"y":0.9415121571757514},{"x":6,"y":0.9302316646425896},{"x":7,"y":0.9190863265105906},{"x":8,"y":0.9080745234610854}]},{"label":"4 Rogues (Average)","dataSeries":[{"x":1,"y":0.99146875},{"x":2,"y":0.9830102822265625},{"x":3,"y":0.9746239757563171},{"x":4,"y":0.9663092149631461},{"x":5,"y":0.9580653894729918},{"x":6,"y":0.9498918941190503},{"x":7,"y":0.9417881288973471},{"x":8,"y":0.9337534989226917}]},{"label":"One of Each","dataSeries":[{"x":1,"y":0.580875625},{"x":2,"y":0.3374164917191406},{"x":3,"y":0.19599701551266313},{"x":4,"y":0.11384988888405288},{"x":5,"y":0.06613262536170478},{"x":6,"y":0.038414830089871115},{"x":7,"y":0.022314238437722688},{"x":8,"y":0.01296179719891119}]}]}"##;

        let outputJSON = WASM_CheckAgainstDC_PassAllProbability::GeneratePlot( &WASM_GetInputForChangeDetectorTests_CheckAgainstDCList(), 10 ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_CheckAgainstDC_FailAllProbability;

    #[wasm_bindgen]
    impl WASM_CheckAgainstDC_FailAllProbability
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList1( PlotParameter_DC("DC") )
        }

        pub fn GeneratePlot( input : &str, aRawDC : Value ) -> Result<String, JsValue>
        {
            let someChecksAgainstDC = DeserializeClientDataVec::<CheckAgainstDC>(input)?;

            let aNumberOfSavesToMake = std::num::NonZeroU8::new(8).unwrap();

            let aDC = DC(aRawDC);

            let plot : Plot2D<u8, Probability> = GeneratePlot2D_EntireDataSeries
            (
                &format!("Probability Of Failing All Checks Against DC {}", aDC),
                "# of checks to fail in a row",
                "Probability of failing every check",
                0.0,
                1.0,
                &someChecksAgainstDC,
                &|aCheckAgainstDC|
                {
                    GenerateDataSeriesFromSequentialTrials
                    (
                        &CalculateSuccessDistribution(aCheckAgainstDC, aDC),
                        _1d0(),
                        aNumberOfSavesToMake,
                        &|accumulatedDistribution : &Die, _currentTrialNumber : u8| { accumulatedDistribution.ChanceOfEQ_Value(0) }
                    )
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_CheckAgainstDC_FailAllProbability()
    {
        let expectedOutputJSON = r##"{"title":"Probability Of Failing All Checks Against DC 10","xAxisLabel":"# of checks to fail in a row","yAxisLabel":"Probability of failing every check","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"1 Rogue","dataSeries":[{"x":1,"y":0.15},{"x":2,"y":0.0225},{"x":3,"y":0.003375},{"x":4,"y":0.00050625},{"x":5,"y":0.0000759375},{"x":6,"y":0.000011390625},{"x":7,"y":1.70859375e-6},{"x":8,"y":2.562890625e-7}]},{"label":"4 Rogues (Majority)","dataSeries":[{"x":1,"y":0.01198125},{"x":2,"y":0.0001435503515625},{"x":3,"y":1.7199126496582031e-6},{"x":4,"y":2.0606703433717346e-8},{"x":5,"y":2.4689406551522596e-10},{"x":6,"y":2.958099522454301e-12},{"x":7,"y":3.5441729903405594e-14},{"x":8,"y":4.2463622640517823e-16}]},{"label":"4 Rogues (Average)","dataSeries":[{"x":1,"y":0.00853125},{"x":2,"y":0.0000727822265625},{"x":3,"y":6.209233703613281e-7},{"x":4,"y":5.2972525033950806e-9},{"x":5,"y":4.519218541958928e-11},{"x":6,"y":3.8554583186087104e-13},{"x":7,"y":3.2891878780630563e-15},{"x":8,"y":2.806088408472545e-17}]},{"label":"One of Each","dataSeries":[{"x":1,"y":0.419124375},{"x":2,"y":0.17566524171914064},{"x":3,"y":0.07362558464475874},{"x":4,"y":0.030858277148244108},{"x":5,"y":0.012933456123334592},{"x":6,"y":0.005420726714282533},{"x":7,"y":0.0022719586961694707},{"x":8,"y":0.0009522332685578442}]}]}"##;

        let outputJSON = WASM_CheckAgainstDC_FailAllProbability::GeneratePlot( &WASM_GetInputForChangeDetectorTests_CheckAgainstDCList(), 10 ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    #[wasm_bindgen]
    pub struct WASM_AbilityCheck_SuccessProbabilityContest;

    #[wasm_bindgen]
    impl WASM_AbilityCheck_SuccessProbabilityContest
    {
        pub fn GetPlotParameters() -> JsValue
        {
            PlotParameterList1( PlotParameter_RollStyle("Opponent Saving Throw Type") )
        }

        pub fn GeneratePlot( input : &str, anOpponentSavingThrowRollStyleStr : &str ) -> Result<String, JsValue>
        {
            let someChecksAgainstDC = DeserializeClientDataVec::<CheckAgainstDC>(input)?;

            // This plot only makes sense for saving throws. However, for convenience we want to
            // allow clients provide the same input data that they can for any of the other
            // plots in this module, to avoid requiring a special case for a single plot.
            // So we take their input and just remove anything that isn't a spell. If this
            // leaves us an empty list then that's ok, the graph will just be empty. It's
            // up to the client to prevent that if they want. Otherwise we have to support
            // optional plots which is more work than we want to deal with right now.
            let someSavingThrows = FilterNonSavingThrows(someChecksAgainstDC);

            let someOpponentSavingThrowModifiers = GetOpponentSavingThrowModifiers();

            let anOpponentSavingThrowRollStyle : RollStyle = DeserializeJSON::<RollStyle>(anOpponentSavingThrowRollStyleStr)?;

            let plot : Plot2D<SavingThrowModifier, Probability> = GeneratePlot2D_PointByPoint
            (
                "Probability Of Winning Contest Against Opponent",
                "Opponent modifier",
                "Probability of beating opponent",
                0.0,
                1.0,
                &someSavingThrows,
                &someOpponentSavingThrowModifiers,
                &|aSavingThrow, &anOpponentSavingThrowModifier|
                {
                    let opponentSavingThrow = SavingThrow::FromMod(anOpponentSavingThrowRollStyle, anOpponentSavingThrowModifier);

                    aSavingThrow.ChanceToWinContest(&opponentSavingThrow)
                }
            );

            Ok( ToJSON(&plot) )
        }
    }

    // The purpose of this test is simply to act as a change detector.
    //
    // We have our hardcoded input and expected output JSON strings and this test
    // just makes sure that they don't change when we don't expect them to.
    #[test]
    fn TestWASM_AbilityCheck_SuccessProbabilityContest()
    {
        let expectedOutputJSON = r#"{"title":"Probability Of Winning Contest Against Opponent","xAxisLabel":"Opponent modifier","yAxisLabel":"Probability of beating opponent","yAxisSuggestedMin":0.0,"yAxisSuggestedMax":1.0,"data2D":[{"label":"1 Rogue","dataSeries":[{"x":-5,"y":0.8875},{"x":-4,"y":0.8625},{"x":-3,"y":0.835},{"x":-2,"y":0.805},{"x":-1,"y":0.7725},{"x":0,"y":0.7375},{"x":1,"y":0.7},{"x":2,"y":0.66},{"x":3,"y":0.6175},{"x":4,"y":0.5725},{"x":5,"y":0.525},{"x":6,"y":0.475},{"x":7,"y":0.4275},{"x":8,"y":0.3825},{"x":9,"y":0.34},{"x":10,"y":0.3},{"x":11,"y":0.2625},{"x":12,"y":0.2275},{"x":13,"y":0.195},{"x":14,"y":0.165},{"x":15,"y":0.1375},{"x":16,"y":0.1125},{"x":17,"y":0.09},{"x":18,"y":0.07},{"x":19,"y":0.0525},{"x":20,"y":0.0375}]}]}"#;

        let outputJSON = WASM_AbilityCheck_SuccessProbabilityContest::GeneratePlot( &WASM_GetInputForChangeDetectorTests_CheckAgainstDCList(), "\"Normal\"" ).unwrap();

        assert_eq!(outputJSON, expectedOutputJSON);
    }
}

// TODO
/*
pub mod PlotGroupCheck
{
    // Maybe make it a single graph with an extra param?

    // Chance that everyone passes vs DC

    // Chance that at least 1 person passes vs DC

    // Chance than 50% passes vs DC

    // Graph for averaging?
    //     if you want stealth to be harder in general or let really bad rolls have more influence, maybe try averaging the group's rolls. A 15, 13, 10 and 5 would become an 11, enough for a dc 10 or higher check, while PHB rules would work for a 12 or lower. This could also let a rogue with expertise contribute more to helping the group pass in some situations.
}
*/
