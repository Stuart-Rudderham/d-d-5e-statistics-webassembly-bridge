#![allow(non_snake_case)]
#![allow(non_camel_case_types)]

mod graph_utils;
mod graph_types;
mod graph_parameters;
mod client_data_deserialization;
mod random_client_data_generation;

use wasm_bindgen::prelude::*;
use git_version::git_version;

#[wasm_bindgen]
pub fn WASM_GetGitVersionString() -> String
{
    const GIT_VERSION: &str = git_version!();
    format!("Git revision \"{}\"", GIT_VERSION)
}