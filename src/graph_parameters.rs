use dnd5e::distribution::*;
use dnd5e::dice::*;
use dnd5e::typedefs::*;

use wasm_bindgen::prelude::*;

use serde::Serialize;

//
// Base structs used to create the parameter UI elements in JS.
//
#[derive(Serialize)]
pub struct PlotCategoryParameterEntry<T>
{
    value       : T,
    description : String,
}

#[derive(Serialize)]
#[serde(tag = "type", content = "data")]
pub enum PlotParameterData<T>
{
    Numeric { initialValue : T                                                         }, /* TODO: Could add min/max as well. */
    Category{ initialValue : T, allPossibleValues : Vec<PlotCategoryParameterEntry<T>> },
}

#[derive(Serialize)]
pub struct PlotParameter<T>
{
    pub labelName : String,

    #[serde(flatten)]
    pub data      : PlotParameterData<T>,
}

//
// Default implementations for different types of plot parameters.
//
// For consistency we want all charts with a given parameter to share the
// same data, but each chart can customize the name to suit its context.
//
pub fn PlotParameter_AC( labelName : &str ) -> PlotParameter<AC>
{
    PlotParameter
    {
        labelName : labelName.to_string(),
        data      : PlotParameterData::Numeric{ initialValue : AC(10) }
    }
}

pub fn PlotParameter_DC( labelName : &str ) -> PlotParameter<DC>
{
    PlotParameter
    {
        labelName : labelName.to_string(),
        data      : PlotParameterData::Numeric{ initialValue : DC(10) }
    }
}

pub fn PlotParameter_SavingThrowModifier( labelName : &str ) -> PlotParameter<SavingThrowModifier>
{
    PlotParameter
    {
        labelName : labelName.to_string(),
        data      : PlotParameterData::Numeric{ initialValue : SavingThrowModifier(0) }
    }
}

pub fn PlotParameter_RollStyle( labelName : &str ) -> PlotParameter<RollStyle>
{
    let GetPlotCategoryParameterEntry = |aRollStyle : RollStyle|
    {
        // Use a match to make sure that we're not missing
        // any new entries if they are added.
        match aRollStyle
        {
            RollStyle::Normal               => PlotCategoryParameterEntry{ value : aRollStyle, description : "Just roll the dice"                 .into() },
            RollStyle::Advantage            => PlotCategoryParameterEntry{ value : aRollStyle, description : "Roll dice twice, take highest"      .into() },
            RollStyle::SuperAdvantage       => PlotCategoryParameterEntry{ value : aRollStyle, description : "Roll dice three times, take highest".into() },
            RollStyle::Disadvantage         => PlotCategoryParameterEntry{ value : aRollStyle, description : "Roll dice twice, take lowest"       .into() },
            RollStyle::Normal_HalflingLucky => PlotCategoryParameterEntry{ value : aRollStyle, description : "Re-roll a 1, one time"              .into() },
        }
    };

    let allRollStyles = vec!
    [
        GetPlotCategoryParameterEntry( RollStyle::Normal               ),
        GetPlotCategoryParameterEntry( RollStyle::Advantage            ),
        GetPlotCategoryParameterEntry( RollStyle::SuperAdvantage       ),
        GetPlotCategoryParameterEntry( RollStyle::Disadvantage         ),
        GetPlotCategoryParameterEntry( RollStyle::Normal_HalflingLucky ),
    ];

    PlotParameter
    {
        labelName : labelName.to_string(),
        data      : PlotParameterData::Category{ initialValue : RollStyle::Normal, allPossibleValues : allRollStyles }
    }
}

pub fn PlotParameter_Health(labelName : &str) -> PlotParameter<Value>
{
    PlotParameter
    {
        labelName : labelName.to_string(),
        data      : PlotParameterData::Numeric{ initialValue : 30 }
    }
}

//
// Wrappers to convert a number of generic params into JS.
//
pub fn PlotParameterList0() -> JsValue
{
    // Explicitly use a 0-length array because the empty tuple "()" gets serialized as null.
    JsValue::from_serde( &[(); 0] ).unwrap()
}

pub fn PlotParameterList1<T : Serialize>( param : PlotParameter<T> ) -> JsValue
{
    // Tuple of length 1.
    JsValue::from_serde( &(param,) ).unwrap()
}

pub fn PlotParameterList2<T1 : Serialize, T2 : Serialize>( param1 : PlotParameter<T1>
                                                         , param2 : PlotParameter<T2> ) -> JsValue
{
    // Tuple of length 2.
    JsValue::from_serde( &(param1, param2) ).unwrap()
}

pub fn PlotParameterList3<T1 : Serialize, T2 : Serialize, T3 : Serialize>( param1 : PlotParameter<T1>
                                                                         , param2 : PlotParameter<T2>
                                                                         , param3 : PlotParameter<T3> ) -> JsValue
{
    // Tuple of length 3.
    JsValue::from_serde( &(param1, param2, param3) ).unwrap()
}

pub fn PlotParameterList4<T1 : Serialize, T2 : Serialize, T3 : Serialize, T4 : Serialize>( param1 : PlotParameter<T1>
                                                                                         , param2 : PlotParameter<T2>
                                                                                         , param3 : PlotParameter<T3>
                                                                                         , param4 : PlotParameter<T4> ) -> JsValue
{
    // Tuple of length 4.
    JsValue::from_serde( &(param1, param2, param3, param4) ).unwrap()
}
