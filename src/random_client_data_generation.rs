use wasm_bindgen::prelude::*;
use rand::prelude::*;

fn BuildOutputListJSON( fieldName : &str, fieldValue : String ) -> String
{
    format!( "[\n  {{ \"name\": \"Random\", \"{}\": {} }}\n]", fieldName, fieldValue )
}

#[wasm_bindgen]
pub fn WASM_GenerateRandomInput_Dice() -> String
{
    let mut rng = rand::thread_rng();

    BuildOutputListJSON( "dice", dnd5e::dice::GenerateRandomJSON_Die(&mut rng) )
}

#[wasm_bindgen]
pub fn WASM_GenerateRandomInput_DamageDealer() -> String
{
    let mut rng = rand::thread_rng();

    if rng.gen()
    {
        BuildOutputListJSON( "Attack", dnd5e::attack_sequence::GenerateRandomJSON_AttackSequence(&mut rng) )
    }
    else
    {
        BuildOutputListJSON( "Spell", dnd5e::spell::GenerateRandomJSON_Spell(&mut rng) )
    }
}

#[wasm_bindgen]
pub fn WASM_GenerateRandomInput_CheckAgainstDC() -> String
{
    let mut rng = rand::thread_rng();

    if rng.gen()
    {
        BuildOutputListJSON( "SavingThrow", dnd5e::saving_throw::GenerateRandomJSON_SavingThrow(&mut rng) )
    }
    else
    {
        BuildOutputListJSON( "GroupCheck", dnd5e::group_check::GenerateRandomJSON_GroupCheck(&mut rng) )
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn TestGenerateRandomInput_Dice()
    {
        for _ in 0..=10_000
        {
            let input = WASM_GenerateRandomInput_Dice();

            let validation = crate::graph_types::PlotDie::WASM_ValidateInput_DiceList(&input);

            assert!( validation.is_ok(), "Input:\n{:?}\n\nValidation:\n{:?}", input, validation);
        }
    }

    #[test]
    fn TestGenerateRandomInput_DamageDealer()
    {
        for _ in 0..=10_000
        {
            let input = WASM_GenerateRandomInput_DamageDealer();

            let validation = crate::graph_types::PlotDamageToCreature::WASM_ValidateInput_DamageDealerList(&input);

            assert!( validation.is_ok(), "Input:\n{:?}\n\nValidation:\n{:?}", input, validation);
        }
    }

    #[test]
    fn TestGenerateRandomInput_CheckAgainstDC()
    {
        for _ in 0..=10_000
        {
            let input = WASM_GenerateRandomInput_CheckAgainstDC();

            let validation = crate::graph_types::PlotCheckAgainstDC::WASM_ValidateInput_CheckAgainstDCList(&input);

            assert!( validation.is_ok(), "Input:\n{:?}\n\nValidation:\n{:?}", input, validation);
        }
    }
}
