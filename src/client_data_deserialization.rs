use wasm_bindgen::prelude::*;

use serde::Deserialize;
use serde::Serialize;

use dnd5e::dice::*;

//
// A helper struct to make it easier to convert JSON data
// from the client into Rust structs that we can operate on.
//
#[derive(Deserialize, Debug)]
pub struct ClientData<T>
{
    name : String,

    // Make the JSON a little less nested and easier to write.
    // https://serde.rs/field-attrs.html#flatten
    #[serde(flatten)]
    data : T
}

pub fn DeserializeJSON<T : serde::de::DeserializeOwned>( input : &str ) -> Result<T, JsValue>
{
    // Return any deserialization errors as a proper
    // struct so the JS can use the data intelligently.
	#[derive(Serialize, Debug)]
	pub struct Error
	{
	    errorMessage : String,
	    inputText    : String,

        // NOTE: The struct json5::Error *does* include the line/column of the error but
        // it *doesn't* make it public, so we can't include it here. Uncomment the below
        // lines if this changes in the future.
	  //line         : usize,
	  //column       : usize,
	}

    let FormatErrorMessage = |jsonError : json5::Error|
    {
        let error = Error
        {
            errorMessage : jsonError.to_string(),
            inputText    : input.to_string(),
          //line         : jsonError.location().map(|x| x.line  ).or_unwrap(0),
          //column       : jsonError.location().map(|x| x.column).or_unwrap(0),
        };

        JsValue::from_serde( &error ).unwrap()
    };

    //
    // If parsing fails then the error will be thrown
    // as an exception on the JavaScript side.
    //
    // We use json5 rather than serde_json because it's nicer for
    // the client. They don't have to worry about small formatting
    // issues like dangling commas.
    //
    json5::from_str(input).map_err(FormatErrorMessage)
}

//
// A helper function for parsing a list of client data from JSON.
//
// Sometimes the type that is most convenient to parse and the type you actually
// need are not the same, this function allows you to provide a conversion function.
//
fn DeserializeClientDataVecThenMap<InputType, OutputType, MappingFn>( input : &str, mappingFn : MappingFn ) -> Result<Vec<(String, OutputType)>, JsValue>
  where InputType : serde::de::DeserializeOwned
      , MappingFn : Fn(InputType) -> OutputType
{
    let clientDataVec : Vec<ClientData<InputType>> = DeserializeJSON(input)?;

    //
    // All the graphing functions take in a tuple but the JSON for parsing
    // a tuple is awkward since you can't give the fields names. So this is
    // a little conversion function.
    //
    // NOTE: If efficiency is a concern then the graphing functions should
    //       be updated to just take a ClientData struct directly.
    //
    Ok( clientDataVec.into_iter().map(|x| (x.name, mappingFn(x.data))).collect() )
}

//
// A wrapper for the most common parsing case where no type conversion is needed.
//
pub fn DeserializeClientDataVec<T : serde::de::DeserializeOwned>( input : &str ) -> Result<Vec<(String, T)>, JsValue>
{
    let identity = |x| { x };

    DeserializeClientDataVecThenMap(input, identity)
}

//
// A wrapper struct for parsing a Die.
//
// This is useful because, unlike all the other objects, you deserialize a Die from
// a single string rather than a map. This means that we can't use ClientData<Die>
// directly because it uses "#[serde(flatten)]" internally which only works with
// structs. Plus, now we can get a nicely named field as well.
//
#[derive(Deserialize, Debug)]
struct ClientDataDie
{
    dice : Die
}

pub fn DeserializeClientDataVec_Die( input : &str ) -> Result<Vec<(String, Die)>, JsValue>
{
    DeserializeClientDataVecThenMap(input, |x : ClientDataDie| { x.dice })
}
