use serde::Serialize;
use dnd5e::dice::*;

//
// Base data structs for a graph.
//
#[derive(Serialize, Debug, Eq, PartialEq)]
pub struct PlotPoint2D<XAxisType, YAxisType>
{
    pub x                 : XAxisType,
    pub y                 : YAxisType,
}

#[derive(Serialize)]
pub struct PlotDataSeries2D<XAxisType, YAxisType>
{
    pub label             : String,
    pub dataSeries        : Vec<PlotPoint2D<XAxisType, YAxisType>>,
}

#[derive(Serialize)]
pub struct Plot2D<XAxisType, YAxisType>
{
    pub title             : String,
    pub xAxisLabel        : String,
    pub yAxisLabel        : String,
    pub yAxisSuggestedMin : YAxisType,
    pub yAxisSuggestedMax : YAxisType,
    pub data2D            : Vec<PlotDataSeries2D<XAxisType, YAxisType>>,
}

//
// Generic function for 2D graph where each data-series is generated with a single function call.
//
pub fn GeneratePlot2D_EntireDataSeries< DataSeriesObject
                                      , XAxisType : Serialize + Clone /*TODO: Copy?*/
                                      , YAxisType : Serialize + Clone /*TODO: Copy?*/ >
                                      ( plotTitle                     : &str
                                      , xAxisLabel                    : &str
                                      , yAxisLabel                    : &str
                                      , yAxisSuggestedMin             : YAxisType
                                      , yAxisSuggestedMax             : YAxisType
                                      , dataSeriesObjectWithLabelList : &[(String, DataSeriesObject)]                                         // Each object we want to graph.
                                      , dataSeriesGeneratorFn         : &dyn Fn(&DataSeriesObject) -> Vec<PlotPoint2D<XAxisType, YAxisType>>) // Generator for all the XY points.
                                      -> Plot2D<XAxisType, YAxisType>
{
    let GeneratePlotDataSeries2D = | &(ref label, ref dataSeriesObject) : &(String, DataSeriesObject) |
    {
        PlotDataSeries2D
        {
            label      : label.to_string(),
            dataSeries : dataSeriesGeneratorFn(dataSeriesObject),
        }
    };

    Plot2D
    {
        title             : plotTitle.to_string(),
        xAxisLabel        : xAxisLabel.to_string(),
        yAxisLabel        : yAxisLabel.to_string(),
        yAxisSuggestedMin,
        yAxisSuggestedMax,
        data2D            : dataSeriesObjectWithLabelList.iter().map(GeneratePlotDataSeries2D).collect(), // For every data object generate the data series.
    }
}

//
// Generic function for 2D graph where each data series is generated with N function calls, one for each individual point.
//
pub fn GeneratePlot2D_PointByPoint< DataSeriesObject
                                  , XAxisType : Serialize + Clone /*TODO: Copy?*/
                                  , YAxisType : Serialize + Clone /*TODO: Copy?*/ >
                                  ( plotTitle                     : &str
                                  , xAxisLabel                    : &str
                                  , yAxisLabel                    : &str
                                  , yAxisSuggestedMin             : YAxisType
                                  , yAxisSuggestedMax             : YAxisType
                                  , dataSeriesObjectWithLabelList : &[(String, DataSeriesObject)]                        // Each object we want to graph.
                                  , xAxisVariableList             : &[XAxisType]                                         // Every value in the x-axis we want to generate a Y coord for.
                                  , yAxisVariableGeneratorFn      : &dyn Fn(&DataSeriesObject, &XAxisType) -> YAxisType) // Generator for a Y coord given an X coord.
                                  -> Plot2D<XAxisType, YAxisType>
{
    // TODO: Sometimes we want to map the x-axis value to another object. For example, x-axis
    //       is saving throw modifiers, but we need to convert them into SavingThrows for the
    //       calculation. It would be great if we could find a way to do this in O(N) time
    //       rather than O(M * N), where N is the number of x-axis values and M is the number
    //       of data series objects.
    let GeneratePlotDataSeries2D = | dataSeriesObject : &DataSeriesObject |
    {
        let GeneratePlotPoint2D = | xAxisVariable : &XAxisType |
        {
            PlotPoint2D
            {
                x : xAxisVariable.clone(),
                y : yAxisVariableGeneratorFn(dataSeriesObject, xAxisVariable)
            }
        };

        xAxisVariableList.iter().map(GeneratePlotPoint2D).collect() // For every tick on the x-axis generate the plot point.
    };

    // Just forward our helper function along to the more flexible implementation.
    GeneratePlot2D_EntireDataSeries
    (
        plotTitle,
        xAxisLabel,
        yAxisLabel,
        yAxisSuggestedMin,
        yAxisSuggestedMax,
        dataSeriesObjectWithLabelList,
        &GeneratePlotDataSeries2D
    )
}

//
// This is a helper function that allows us to optimize the creation of a data
// series if each entry in the series can be derived from the previous one.
//
// TODO: Add ability to skip/ignore the first X rounds. For example, this is useful 
//       if you need to spend the first round of a fight casting a buff spell.
//
//       Concrete usecase, WASM_Creature_TurnsToKill but want to spend the first round
//       casting darkness to get advantage on all future attacks.
//
pub fn GenerateDataSeriesFromSequentialTrials< YAxisType : Clone >
                                             ( distributionForOneTrial      : &Die
                                             , mut accumulatingDistribution : Die
                                             , numTotalTrials               : std::num::NonZeroU8
                                             , calculateValueAfterNTrialsFn : &dyn Fn(&Die, u8) -> YAxisType )
                                             -> Vec<PlotPoint2D<u8, YAxisType>>
{
    let CalculateValueAfterNTrials = |currentTrialNumber : u8|
    {
        // Modify the existing distribution rather than creating a new one with "* currentTrialNumber"
        // to reduce the number of overall additions we have to do. This way we do O(n) additions
        // rather than "1 * 2 * 3 ... * N" additions with is O(n^2).
        //
        // NOTE: This optimization is only correct because we are always a plotting every trial count
        // within the desired ranged. If we wanted to do something like "plot for every other trial" then
        // everything would be wrong since we wouldn't accumulate enough during each function call.
        accumulatingDistribution += distributionForOneTrial;

        let afterNTrials = calculateValueAfterNTrialsFn(&accumulatingDistribution, currentTrialNumber);

        PlotPoint2D
        {
            x : currentTrialNumber,
            y : afterNTrials
        }
    };

    // NOTE: We must always start at 1 for correctness.
    let trialsList = 1..=numTotalTrials.get();

    trialsList.map(CalculateValueAfterNTrials).collect()
}

//
// Helper function to make it easier to convert a type to a JSON string.
//
pub fn ToJSON<T: Serialize>(value: &T) -> String
{
    // Pretty printing makes the generated JSON *much* larger in size (all the newlines + indenting).
    // Mostly useful only for debug.
    let shouldPrettyPrint = false;

    if shouldPrettyPrint
    {
        serde_json::to_string_pretty(value).unwrap()
    }
    else
    {
        //
        // Even though we use the json5 crate for deserialization continue to use
        // serde_json for serialization.
        //
        // This is because we use JsValue::from_serde elsewhere in the project and
        // that uses serde_json under the hood so this keeps everything consistent.
        //
        // Plus it's already included in the library so we're not paying any extra
        // cost in binary size.
        //
        serde_json::to_string(value).unwrap()
    }
}

#[cfg(test)]
mod tests
{
    use super::*;

    #[test]
    fn TestGenerateDataSeriesFromSequentialTrials()
    {
        // We either do miss and do 0 damage, or hit and do 1 damage, with 50% probability each time.
        let damageDistribution = Die::from(0..=1);

        let numTrials = std::num::NonZeroU8::new(4).unwrap();

        // The person we're trying to damage only has 1 health, so the only
        // way we can't defeat them is if we miss with every single attack.
        let enemyHealth = 1;

        let result = GenerateDataSeriesFromSequentialTrials
        (
            &damageDistribution,
            _1d0(),
            numTrials,
            &|accumulatedDistribution : &Die, _currentTrialNumber : u8| { accumulatedDistribution.ChanceOfGE_Value(enemyHealth) }
        );

        // We have a 50% chance to hit, so the odds that we miss X times in a row are "1 - 0.5^X".
        let expected = vec!
        [
            PlotPoint2D{ x : 1, y : 1.0 - 0.5    },
            PlotPoint2D{ x : 2, y : 1.0 - 0.25   },
            PlotPoint2D{ x : 3, y : 1.0 - 0.125  },
            PlotPoint2D{ x : 4, y : 1.0 - 0.0625 },
        ];

        assert_eq!(result, expected);
    }
}

