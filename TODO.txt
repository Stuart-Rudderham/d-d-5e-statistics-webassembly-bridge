- compress (gzip, etc...) .wasm *before* converting to Base64
    - if we do it after then it doesn't compress nearly as well
    - drastically reduces the size
    - need some way to decompress it inside of JS though
        gzip -k      \
             --best  \
             --force \
             pkg/d_d_5e_dice_statistics_wasm_bridge_autogen_bg_opt.wasm
    - might not be needed for server usage, since they compress the JS there already

- Look into using async to get the chart data so we don't block the UI thread for so long

- Try to reduce the manual to/from JSON conversions
    - Look into using wasm_bindgen::JsValue as a rust param
    - https://rustwasm.github.io/docs/wasm-bindgen/reference/arbitrary-data-with-serde.html
    - https://docs.rs/wasm-bindgen/0.2.60/wasm_bindgen/struct.JsValue.html#method.from_serde
    - can put #[wasm_bindgen] on a struct? https://rustwasm.github.io/docs/wasm-bindgen/reference/types/exported-rust-types.html
    - Maybe Result<T, JsValue> to get better errors? https://rustwasm.github.io/docs/wasm-bindgen/reference/types/result.html

- Investigate using plotly

- https://knockoutjs.com/index.html

- Find a JS minifier that actually works
    - YUI Compressor can't handle async/await

Things I don't like about ChartJS
    Tooltips are wrong if the x-axis values for each distribution aren't exactly the same
    axis grids can have weird values, would be nice to be able to force always integer/category
    if a data series is hidden and we re-update the plot then it gets unhidden

